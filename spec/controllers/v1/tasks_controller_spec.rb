require 'rails_helper'

describe V1::TasksController, :type => :controller do

  before do
    authenticate_user
    @list = FactoryGirl.create(:list, user_id: @user.id)
  end

  describe "GET 'tasks'/'list_tasks'" do
    it ' should return HTTP status 200' do
      get :list_tasks, params: { list_id: @list.id }, format: :json
      expect(response).to have_http_status (:success)
    end

    describe 'should return all tasks in list for current user: ' do
      it '0 tasks' do
        get :list_tasks, params: { list_id: @list.id }, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks.size).to eq(0)
      end
      it '1 tasks' do
        FactoryGirl.create(:task, list_id: @list.id)
        get :list_tasks, params: { list_id: @list.id }, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks.size).to eq(1)
      end
      it '5 tasks' do
        FactoryGirl.create_list(:task, 5, list_id: @list.id)
        get :list_tasks, params: { list_id: @list.id }, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks.size).to eq(5)
      end
    end

    describe 'should return all tasks' do
      it ' ordered by position' do
        task1 = FactoryGirl.create(:task, list_id: @list.id, position: 1)
        task2 = FactoryGirl.create(:task, list_id: @list.id, position: 2)
        task3 = FactoryGirl.create(:task, list_id: @list.id, position: 3)
        task4 = FactoryGirl.create(:task, list_id: @list.id, position: 4)
        patch :up_position, params: { id: task3.id }, format: :json
        patch :up_position, params: { id: task3.id }, format: :json
        get :list_tasks, params: { list_id: @list.id }, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks[0]['id']).to eq(task3.id)
        expect(tasks[1]['id']).to eq(task1.id)
        expect(tasks[2]['id']).to eq(task2.id)
        expect(tasks[3]['id']).to eq(task4.id)
      end
    end
  end

  describe "GET 'tasks'/'index'" do
    it ' should return HTTP status 200' do
      get :index, format: :json
      expect(response).to have_http_status (:success)
    end

    describe 'should return all tasks in list for current user: ' do
      it '0 tasks' do
        get :index, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks.size).to eq(0)
      end
      it '1 tasks' do
        FactoryGirl.create(:task, list_id: @list.id)
        get :index, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks.size).to eq(1)
      end
      it '5 tasks' do
        FactoryGirl.create_list(:task, 5, list_id: @list.id)
        get :index, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks.size).to eq(5)
      end
    end
  end


  describe "POST 'task'/'create'" do
    it ' should return HTTP status 200' do
      post :create, params: { task: { content: 'test content', list_id: @list.id } }, format: :json
      expect(response).to have_http_status (:created)
    end

    it ' should not create task with empty content' do
      post :create, params: { task: { content: '', list_id: @list.id } }, format: :json
      expect(response).to have_http_status (:unprocessable_entity)
    end

    describe 'should create tasks: ' do
      it '1 task' do
        post :create, params: { task: { content: 'test content', list_id: @list.id } }, format: :json
        get :list_tasks, params: { list_id: @list.id }, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks.size).to eq(1)
      end

      it '5 tasks' do
        (0..4).each do |i|
          post :create, params: { task: { content: "content #{i}", list_id: @list.id } }, format: :json
        end
        get :list_tasks, params: { list_id: @list.id }, format: :json
        expect(JSON.parse(response.body).size).to eq(5)
      end
    end

    describe 'should set proper position: ' do
      it '1 task' do
        post :create, params: { task: { content: 'test content', list_id: @list.id } }, format: :json
        get :list_tasks, params: { list_id: @list.id }, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks[0]['position']).to eq(1)
      end

      it '4 tasks' do
        (0..4).each do |i|
          post :create, params: { task: { content: "content #{i}", list_id: @list.id } }, format: :json
        end
        get :list_tasks, params: { list_id: @list.id }, format: :json
        tasks = JSON.parse(response.body)
        expect(tasks[0]['position']).to eq(1)
        expect(tasks[1]['position']).to eq(2)
        expect(tasks[2]['position']).to eq(3)
        expect(tasks[3]['position']).to eq(4)

      end
    end
  end

  describe "PATCH 'task'/'update'" do
    it ' should return HTTP status 200' do
      task = FactoryGirl.create(:task, list_id: @list.id)
      patch :update, params: { id: task.id, task: { content: 'updated content', list_id: @list.id } }, format: :json
      expect(response).to have_http_status (:success)
    end

    it ' should update task content' do
      task = FactoryGirl.create(:task, list_id: @list.id)
      patch :update, params: { id: task.id, task: { content: 'updated content', list_id: @list.id } }, format: :json
      expect(JSON.parse(response.body)['content']).to eq('updated content')
    end

    it ' should not update task content to empty' do
      task = FactoryGirl.create(:task, list_id: @list.id)
      patch :update, params: { id: task.id, task: { content: '' }, list_id: @list.id }, format: :json
      expect(response).to have_http_status (:unprocessable_entity)
    end
  end

  describe "PATCH 'task'/'check'" do
    it ' should return HTTP status 200' do
      task = FactoryGirl.create(:task, list_id: @list.id)
      patch :check, params: { id: task.id, list_id: @list.id }, format: :json
      expect(response).to have_http_status (:success)
    end

    it ' should check task' do
      task = FactoryGirl.create(:task, list_id: @list.id)
      patch :check, params: { id: task.id, list_id: @list.id}, format: :json
      expect(JSON.parse(response.body)['is_done']).to eq(true)
    end

    it ' should uncheck task' do
      task = FactoryGirl.create(:task, list_id: @list.id)
      patch :check, params: { id: task.id, list_id: @list.id }, format: :json
      patch :check, params: { id: task.id, list_id: @list.id }, format: :json
      expect(JSON.parse(response.body)['is_done']).to eq(false)
    end
  end

  describe "PATCH 'task'/'up_position'" do
    it ' should return HTTP status 200' do
      FactoryGirl.create(:task, list_id: @list.id, position: 1)
      task2 = FactoryGirl.create(:task, list_id: @list.id, position: 2)
      patch :up_position, params: { id: task2.id }, format: :json
      expect(response).to have_http_status(:success)
    end

    it ' should swap second and first position' do
      task1 = FactoryGirl.create(:task, list_id: @list.id, position: 1)
      task2 = FactoryGirl.create(:task, list_id: @list.id, position: 2)
      patch :up_position, params: { id: task2.id }, format: :json
      tasks = JSON.parse(response.body)
      expect(tasks[0]['id']).to eq(task2.id)
      expect(tasks[1]['id']).to eq(task1.id)
    end

    it ' should do nothing' do
      task1 = FactoryGirl.create(:task, list_id: @list.id, position: 1)
      patch :up_position, params: { id: task1.id }, format: :json
      expect(response).to have_http_status (:unprocessable_entity)
    end

    it ' should swap third and second position' do
      task1 = FactoryGirl.create(:task, list_id: @list.id, position: 1)
      task2 = FactoryGirl.create(:task, list_id: @list.id, position: 2)
      task3 = FactoryGirl.create(:task, list_id: @list.id, position: 3)
      task4 = FactoryGirl.create(:task, list_id: @list.id, position: 4)
      patch :up_position, params: { id: task3.id }, format: :json
      tasks = JSON.parse(response.body)
      expect(tasks[0]['id']).to eq(task1.id)
      expect(tasks[1]['id']).to eq(task3.id)
      expect(tasks[2]['id']).to eq(task2.id)
      expect(tasks[3]['id']).to eq(task4.id)
    end
  end

  describe "PATCH 'task'/'down_position'" do
    it ' should return HTTP status 200' do
      task1 = FactoryGirl.create(:task, list_id: @list.id, position: 1)
      FactoryGirl.create(:task, list_id: @list.id, position: 2)
      patch :down_position, params: { id: task1.id }, format: :json
      expect(response).to have_http_status(:success)
    end

    it ' should swap second and first position' do
      task1 = FactoryGirl.create(:task, list_id: @list.id, position: 1)
      task2 = FactoryGirl.create(:task, list_id: @list.id, position: 2)
      patch :down_position, params: { id: task1.id }, format: :json
      tasks = JSON.parse(response.body)
      expect(tasks[0]['id']).to eq(task2.id)
      expect(tasks[1]['id']).to eq(task1.id)
    end

    it ' should do nothing ' do
      task1 = FactoryGirl.create(:task, list_id: @list.id, position: 1)
      patch :down_position, params: { id: task1.id }, format: :json
      expect(response).to have_http_status (:unprocessable_entity)
    end

    it ' should swap third and second position' do
      task1 = FactoryGirl.create(:task, list_id: @list.id, position: 1)
      task2 = FactoryGirl.create(:task, list_id: @list.id, position: 2)
      task3 = FactoryGirl.create(:task, list_id: @list.id, position: 3)
      task4 = FactoryGirl.create(:task, list_id: @list.id, position: 4)
      patch :down_position, params: { id: task2.id }, format: :json
      tasks = JSON.parse(response.body)
      expect(tasks[0]['id']).to eq(task1.id)
      expect(tasks[1]['id']).to eq(task3.id)
      expect(tasks[2]['id']).to eq(task2.id)
      expect(tasks[3]['id']).to eq(task4.id)
    end
  end

  describe "DELETE 'task'/'destroy'" do
    it ' should destroy task' do
      task = FactoryGirl.create(:task, list_id: @list.id)
      delete :destroy, params: { id: task.id, list_id: @list.id }, format: :json
      expect(response).to have_http_status (:success)
    end

    it ' should destroy task and return him' do
      task = FactoryGirl.create(:task, list_id: @list.id, content: 'first')
      delete :destroy, params: { id: task.id, list_id: @list.id }, format: :json
      returned_task = JSON.parse(response.body)
      expect(returned_task['id']).to eq (task.id)
      expect(returned_task['content']).to eq ('first')
    end

    it ' should not destroy not existed task' do
      delete :destroy, params: { id: -1, list_id: @list.id }, format: :json
      expect(response).to have_http_status (:unprocessable_entity)
    end

    it ' should update task positions' do
      FactoryGirl.create(:task, list_id: @list.id, position: 1)
      FactoryGirl.create(:task, list_id: @list.id, position: 2)
      task3 = FactoryGirl.create(:task, list_id: @list.id, position: 3)
      FactoryGirl.create(:task, list_id: @list.id, position: 4)
      patch :destroy, params: { id: task3.id }, format: :json
      get :list_tasks, params: { list_id: @list.id }, format: :json
      tasks = JSON.parse(response.body)
      expect(tasks[0]['position']).to eq(1)
      expect(tasks[1]['position']).to eq(2)
      expect(tasks[2]['position']).to eq(3)
    end
  end
end