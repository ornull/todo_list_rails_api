Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount_devise_token_auth_for 'User', at: 'auth'
  namespace :v1 do
    resources :lists, except: [:show]
    resources :tasks, except: [:show]
    patch 'tasks/:id/up' => 'tasks#up_position', as: :up_position
    patch 'tasks/:id/down' => 'tasks#down_position', as: :down_position
    patch 'tasks/:id/check' => 'tasks#check', as: :check
    get 'list_tasks/:list_id' => 'tasks#list_tasks', as: :list_tasks
  end
end
