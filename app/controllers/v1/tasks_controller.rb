class V1::TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :find_task, only: [:update, :destroy, :check, :up_position, :down_position]
  before_action :find_list, only: [:list_tasks]

  def index
    render json: Task.all, status: :ok
  end

  def list_tasks
    tasks = @list.tasks.order_tasks
    render json: tasks, status: :ok
  end

  def create
    task = Task.new(task_params)
    list = List.find_by(id: task.list_id)
    task.position = list.tasks.count + 1
    if task.save
      render json: task, status: :created
    else
      render json: task, status: :unprocessable_entity
    end
  end

  def update
    if @task.update(task_params)
      render json: @task, status: :ok
    else
      render json: @task, status: :unprocessable_entity
    end
  end

  def destroy
    if @task && @task.destroy
      render json: @task, status: :ok
    else
      render json: @task, status: :unprocessable_entity
    end
  end

  def up_position
    if @task.position != 1
      list = List.find_by(id: @task.list_id)
      @task.swap_positions(@task.position - 1)
      render json: list.tasks.order_tasks, status: :ok
    else
      render json: @task, status: :unprocessable_entity
    end
  end

  def down_position
    list = List.find_by(id: @task.list_id)
    if @task.position != list.tasks.count
      @task.swap_positions( @task.position + 1)
      render json: list.tasks.order_tasks, status: :ok
    else
      render json: @task, status: :unprocessable_entity
    end
  end

  def check
    if @task.update(is_done: !@task.is_done)
      render json: @task, status: :ok
    end
  end

  private

  def task_params
    params.require(:task).permit(:content, :list_id)
  end

  def find_task
    @task = Task.find_by(id: params[:id])
  end

  def find_list
    @list = List.find_by(id: params[:list_id])
  end
end
